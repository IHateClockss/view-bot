FROM gitpod/workspace-full-vnc

# update
RUN sudo apt-get update && sudo apt-get upgrade -y

# setup vnc
RUN sudo apt-get update && \
    sudo apt-get install -y libx11-dev libxkbfile-dev libsecret-1-dev libnss3 && \
    sudo apt-get update && \
    sudo apt-get install -y libgconf-2-4 && \
    sudo rm -rf /var/lib/apt/lists/*

# get latest version of java
RUN wget https://download.oracle.com/java/20/latest/jdk-20_linux-x64_bin.tar.gz && \
    sudo mkdir /usr/lib/jvm && \
    sudo tar -xzf jdk-* -C /usr/lib/jvm  && \
    echo -e "$(cat ~/.bashrc)\nexport JAVA_HOME=\"/usr/lib/jvm/jdk-20.0.1/bin\"\nexport PATH=\"\$JAVA_HOME:\$PATH\"\n" > ~/.bashrc && \
    sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk-20.0.1/bin/java" 0 && \
    sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk-20.0.1/bin/javac" 0 && \
    sudo update-alternatives --set java /usr/lib/jvm/jdk-20.0.1/bin/java && \
    sudo update-alternatives --set javac /usr/lib/jvm/jdk-20.0.1/bin/javac && \
    rm -rf jdk-*

# get latest version of python
RUN wget https://www.python.org/ftp/python/3.11.4/Python-3.11.4.tgz && \
    sudo apt update && sudo apt upgrade -y && \
    sudo apt-get install -y make build-essential libssl-dev zlib1g-dev \
        libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
        libncurses5-dev libncursesw5-dev xz-utils tk-dev && \
    tar -xzf Python-3.11.4.tgz && \
    cd Python-3.11.4 && \
    ./configure --enable-optimizations --with-ensurepip=install && \
    make -j 8 && \
    sudo make altinstall && \
    cd .. && \
    sudo rm -rf Python-3.11.4 && \
    rm Python-3.11.4.tgz && \
    echo -e "$(cat ~/.bashrc)\nexport PATH=\"/usr/local/bin:\$PATH\"\nalias py=\"python3.11\"" > ~/.bashrc

# node stuff
RUN nvm install --lts && \
    nvm install node && \
    nvm use --lts && \
    npm i -g npm yarn pnpm

# rust stuff
RUN rustup set profile complete && \
    rustup toolchain install --force nightly && \
    rustup default nightly-x86_64-unknown-linux-gnu
